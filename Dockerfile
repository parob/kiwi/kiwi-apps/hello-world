FROM frolvlad/alpine-python3:latest

MAINTAINER Rob Parker <rob@parob.com>

COPY requirements.txt ./

RUN pip install -r requirements.txt

COPY . .

EXPOSE 8080

CMD gunicorn -b :8080 main:app
