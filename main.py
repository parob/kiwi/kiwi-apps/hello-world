from objectql.schema import ObjectQLSchema
from werkzeug_graphql import GraphQLAdapter

schema = ObjectQLSchema()

@schema.root
class HelloWorld:

    @schema.query
    def hello_world(self) -> str:
        return "Hello world!"


adapter = GraphQLAdapter.from_schema(schema=schema)


def main(request):
    return adapter.dispatch(request=request)


app = adapter.application(main=main)

if __name__ == "__main__":
    adapter.run_app(port=3502)
